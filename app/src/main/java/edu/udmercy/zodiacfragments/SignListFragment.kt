package edu.udmercy.zodiacfragments

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

private const val TAG = "SignListFragment"

class SignListFragment : Fragment() {

    interface Callbacks {
        fun onSignSelected(signId: Int)
    }

    private lateinit var signRecyclerView: RecyclerView
    private var adapter: SignAdapter? = SignAdapter(emptyList())
    private var callbacks: Callbacks? = null


    private val signViewModel: SignViewModel by lazy {
        ViewModelProviders.of(this).get(SignViewModel::class.java)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        callbacks = context as Callbacks?
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_sign_list, container, false)

        signRecyclerView = view.findViewById(R.id.sign_recycler_view) as RecyclerView
        signRecyclerView.layoutManager = LinearLayoutManager(context)
        signRecyclerView.adapter = adapter
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        signViewModel.signListLiveData.observe(
            viewLifecycleOwner,
            Observer { signs ->
                signs?.let {
                    Log.i(TAG, "Got signs ${signs.size}")
                    updateUI(signs)
                }
            })
    }
    private fun updateUI(signs: List<Sign>) {
        adapter = SignAdapter(signs)
        signRecyclerView.adapter = adapter
    }

    private inner class SignAdapter(var signs: List<Sign>)
        : RecyclerView.Adapter<SignHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int)
                : SignHolder {
            val view = layoutInflater.inflate(R.layout.list_item_sign, parent, false)
            return SignHolder(view)
        }

        override fun getItemCount() = signs.size

        override fun onBindViewHolder(holder: SignHolder, position: Int) {
            val sign = signs[position]
            holder.bind(sign)
        }
    }


    private inner class SignHolder(view: View) : RecyclerView.ViewHolder(view), View.OnClickListener {
        private lateinit var sign: Sign

        val signTextView: TextView = itemView.findViewById(R.id.sign_name)

        init {
            itemView.setOnClickListener(this)
        }

        fun bind(sign: Sign) {
            this.sign = sign
            signTextView.text = this.sign.name
        }

        override fun onClick(v: View) {
            callbacks?.onSignSelected(sign.id)
            // Toast.makeText(context, "${sign.name} pressed", Toast.LENGTH_SHORT).show()
        }
    }

    override fun onDetach() {
        super.onDetach()
        callbacks = null
    }

    companion object {
        fun newInstance(): SignListFragment {
            return SignListFragment()
        }
    }
}

