package edu.udmercy.zodiacfragments

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import kotlin.math.sign

class SignViewModel : ViewModel() {

    private val signsRepository = SignRepository.get()
    val signListLiveData = signsRepository.getSigns()

    private val signsIdLiveData = MutableLiveData<Int>()

    var signsLiveData: LiveData<Sign?> =
        Transformations.switchMap(signsIdLiveData) { signId ->
            signsRepository.getSign(signId)
        }

    fun loadSign(signId: Int) {
        signsIdLiveData.value = signId
    }
}